﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signature
{
    class Program
    {
        static HashGenerator hashG = new HashGenerator();

        static void Main(string[] args)
        {
            bool flag = true;

            while (flag)
            {
                string select = Console.ReadLine();
                switch (select)
                {
                    case "start":
                        {
                            StartGeneration();
                        };
                        break;
                    case "r":
                        {
                            hashG.Progress("read");
                        };
                        break;
                    case "w":
                        {
                            hashG.Progress("write");
                        };
                        break;
                    case "s":
                        {
                            hashG.Progress("sha");
                        };
                        break;
                    case "end":
                        {
                            flag = false;
                        };
                        break;
                }
            }  
        }

        static void StartGeneration()
        {

            //hashG.StartCreatingHash(@"D:\Work\New folder\CSV\pr\temp27.csv", @"D:\Work\New folder\CSV\pr\temp4.txt", 100000);

            Console.WriteLine("Enter the path to the file");
            string pathOld = Console.ReadLine();

            if (System.IO.File.Exists(pathOld))
            {
                Console.WriteLine("Enter the path to the new file with hash");
                string pathNew = Console.ReadLine();

                try
                {
                    Console.WriteLine("Enter the block size");
                    int blockSize;

                    blockSize = int.Parse(Console.ReadLine());
                    hashG.StartCreatingHash(pathOld, pathNew, blockSize);
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }

            }
            else
            {
                Console.WriteLine("The file does not exist ");
            }
        }
    }
}
