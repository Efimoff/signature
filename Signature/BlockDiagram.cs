﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signature
{   
    // Так ли нужно объявлять структуру в пустом классе? 
    // Какой в этом смысл?
    class BlockDiagram
    {
        public struct Block
        {
            public double number;
            public int bytes;
            public byte[] block;
        }
    }
}
