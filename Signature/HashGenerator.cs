﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Signature
{
    // Вообще этот класс делает 3 разные функции
    // руководстуяь принципом единственной ответстветнности
    // можно разбить этот класс на 3 класса и организавать 
    // между ними передачу данных. А какой-нибудь верхнеуровневый
    // класс будет их инициализировать и запускать.
    class HashGenerator
    {
        private int bufferLength;
        private string sourceFile;
        private string destinationFile;
        private bool flag;
        private byte[] streamBuffer;

        private Queue<BlockDiagram.Block> queueRead = new Queue<BlockDiagram.Block>();
        private Queue<BlockDiagram.Block> queueHash = new Queue<BlockDiagram.Block>();

        private double countBlock = 0;
        private double numRead = 0;
        private double numWrite = 0;
        private double numHash = 0;


        public void StartCreatingHash(string sFile, string dFile, int bLength)
        {
            sourceFile = sFile;
            destinationFile = dFile;
            flag = true;

            bufferLength = bLength;
            streamBuffer = new byte[bufferLength];

            Thread threadRead = new Thread(() =>
            {
                ReadBlock();
            });
            Thread threadWrite = new Thread(() =>
            {
                WriteBlock();
            });
            Thread threadSHA = new Thread(() =>
            {
                ProcessHashes();
            });

            threadRead.Start();
            threadWrite.Start();
            threadSHA.Start();
        }

        public void ReadBlock()
        {
            using (FileStream sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
            {
                double sLength = sourceStream.Length;
                countBlock = Math.Ceiling(sLength / bufferLength);
                bool flagRead = flag;

                while (flagRead)
                {
                    if (queueRead.Count < 4)
                    {
                        numRead++;
                        var newBlock = ReadNewBlock(sourceStream, numRead);

                        lock (queueRead)
                        {
                            queueRead.Enqueue(newBlock);
                        }

                        if (countBlock == numRead)
                        {
                            flagRead = false;
                        }
                    }
                }
            }
            numRead = 0;
        }

        private BlockDiagram.Block ReadNewBlock(Stream source, double numReads)
        {
            BlockDiagram.Block newBlock = new BlockDiagram.Block();

            if (numReads > countBlock - 4)
            {
                Console.WriteLine(numReads);
            }

            newBlock.bytes = source.Read(streamBuffer, 0, bufferLength);
            newBlock.block = streamBuffer;
            newBlock.number = numReads;

            return newBlock;
        }

        public void WriteBlock()
        {
            using (StreamWriter destinationStream = new StreamWriter(destinationFile, false, Encoding.Default))
            {
                bool flagWrite = flag;

                while (flagWrite)
                {
                    if (queueHash.Count != 0)
                    {
                        var writeBlock = GetHashBlock();

                        Console.WriteLine("Block {0}", writeBlock.number);
                        Console.WriteLine(BitConverter.ToString(writeBlock.block));

                        destinationStream.Write(BitConverter.ToString(writeBlock.block) + "\n");

                        numWrite = writeBlock.number;

                        if (numWrite == countBlock)
                        {
                            Console.WriteLine("Total blocks: {0}", countBlock);
                            flagWrite = false;
                        }
                    }
                }
            }
            numWrite = 0;
        }

        private BlockDiagram.Block GetHashBlock()
        {
            BlockDiagram.Block shaBlock = new BlockDiagram.Block();
            lock (queueHash)
            {
                shaBlock = queueHash.Dequeue();
            }
            return shaBlock;
        }

        public void ProcessHashes()
        {
            bool flagCreate = flag;

            while (flagCreate)
            {
                if (queueHash.Count < 4 && queueRead.Count != 0)
                {
                    BlockDiagram.Block block = new BlockDiagram.Block();

                    lock (queueRead)
                    {
                        block = queueRead.Dequeue();
                    }

                    SHA256 sha = SHA256.Create();
                    var hash = CreateHash(block, sha);

                    lock (queueHash)
                    {
                        queueHash.Enqueue(hash);
                    }

                    numHash = hash.number;

                    if (numHash == countBlock)
                    {
                        flagCreate = false;
                    }
                }
            }
            Console.WriteLine("The signature generation process was successful");
            numHash = 0;
        }

        private BlockDiagram.Block CreateHash(BlockDiagram.Block block, SHA256 sha)
        {
            //SHA256 sha = SHA256.Create();
            byte[] hash = sha.ComputeHash(block.block);

            BlockDiagram.Block shaBlock = new BlockDiagram.Block();
            shaBlock.block = hash;
            shaBlock.bytes = hash.Length;
            shaBlock.number = block.number;

            return shaBlock;
        }

        public void Progress(string nameProcess)
        {
            if ("sha" == nameProcess)
            {
                double progress = Math.Round((numHash / countBlock * 100), 2);
                Console.WriteLine("Hash progress: {0}%", progress);
            }
            else if ("write" == nameProcess)
            {
                double progress = Math.Round((numWrite / countBlock * 100), 2);
                Console.WriteLine("Writing progress: {0}%", progress);
            }
            else if ("read" == nameProcess)
            {
                double progress = Math.Round((numRead / countBlock * 100), 2);
                Console.WriteLine("Reading progress: {0}%", progress);
            }
        }
    }
}
